from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from django.contrib.auth import login, authenticate, logout
from django.template import RequestContext
from mongoengine.django.auth import User
from auth.forms import UserRegisterForm, UserLoginForm

def login_user(request):
    """
    Logs-in the user if a POST request is given.
    Renders a login page if a GET request is given.
    """
    # If the user is already authenticated no need to show the login page
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('list_contacts'))
    if request.method == 'POST':
        form = UserLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect(reverse('list_contacts'))
            else:
                return render_to_response(
                        'auth/login_user.html',
                        {'form': form, 'error': 'Invalid username and password'},
                        context_instance=RequestContext(request)
                                        )
    form = UserLoginForm()
    return render_to_response(
                'auth/login_user.html',
                {'form': form, 'error': ''},
                context_instance=RequestContext(request)
                                )

def create_user(request):
    """
    Creates an instance of mongodb User if POST request is given.
    Renders the user signup page if GET request is given"""
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            user = form.save() 
            return HttpResponseRedirect(reverse("login_user"))
    else:
        form = UserRegisterForm()
        return render_to_response(
                                'auth/create_user.html',
                                {'form': form},
                                context_instance=RequestContext(request)
                                )
def logout_user(request):
    """
    Logouts the user and redirects to login page.
    """
    logout(request)
    return HttpResponseRedirect('/list_contacts/')
