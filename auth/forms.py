from django import forms
from mongoengine.django.auth import User

class UserLoginForm(forms.Form):
    """
    The form to be rendered in sign in page.
    """
    username = forms.RegexField(regex='^\w+$',
                                max_length=30,
                                label='username'
                                )
    password = forms.CharField(widget=forms.PasswordInput(render_value=False),
                               label='password'
                               )

class UserRegisterForm(forms.Form):
    """
    The forms to be rendered in sign up page.
    """
    username = forms.RegexField(regex='^\w+$',
                                max_length=30,
                                label='username'
                                )
    email = forms.EmailField()
    password1 = forms.CharField(widget=forms.PasswordInput(render_value=False),
                                label='password'
                                )
    password2 = forms.CharField(widget=forms.PasswordInput(render_value=False),
                                label='password(again)'
                                )

    def clean_username(self):
        """
        Checks if the entered username already exists and raises an error if there is.
        """
        if User.objects(username__iexact=self.cleaned_data['username']):
            raise Form.ValidationError('Choose another username.')
        return self.cleaned_data['username']

    def clean(self):
        """
        Checks if both passwords entered are same.
        """
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise form.ValidationError('Both passwords should be same')
        return self.cleaned_data

    def save(self):
        """
        Creates an instance of mongoengine User.
        """
        user = User.create_user(username=self.cleaned_data['username'],
                                        password=self.cleaned_data['password1'],
                                        email=self.cleaned_data['email']
                                        )
        user.is_active = True
        user.save()
        return user
