from django.conf.urls import *
from auth.views import create_user, login_user

urlpattern = patterns('',
        (r'^create/$', create_user, name='create_user'),
        (r'^login/$', login_user, name='login_user'),
        )
