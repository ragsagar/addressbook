from django.conf.urls import patterns, include, url
from mqbook.views import add_contact, list_contacts
from mqbook.views import delete_contact, update_contact

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'addressbook.views.home', name='home'),
    # url(r'^addressbook/', include('addressbook.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^add_contact/$', add_contact, name='add_contact'),
    url(r'^list_contacts/$', list_contacts, name='list_contacts'),
    url(r'^delete_contact/$', delete_contact, name='delete_contact'),
    url(r'^update_contact/$', update_contact, name='update_contact'),
    url(r'^user/create/$', 'auth.views.create_user', name='create_user'),
    url(r'^user/login/$', 'auth.views.login_user', name='login_user'),
    url(r'^user/logout/$', 'auth.views.logout_user', name='logout_user'),
)
