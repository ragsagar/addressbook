from django import forms
from mqbook.documents import AddressBook

class AddContactForm(forms.Form):
    """Form for adding a new contact to the address book """
    first_name = forms.CharField(max_length=30,required=True)
    last_name = forms.CharField(max_length=30)
    mobile_number = forms.IntegerField()
    email = forms.EmailField(max_length=75)

    def save(self):
        """Creates a new contact and returns it """
        new_contact = AddressBook(first_name=self.cleaned_data['first_name'],
                                  last_name=self.cleaned_data['last_name'],
                                  mobile_number=self.cleaned_data['mobile_number'],
                                  email=self.cleaned_data['email'])
        new_contact.save()
        return new_contact




    
