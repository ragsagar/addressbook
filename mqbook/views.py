from mqbook.documents import AddressBook
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from mqbook.forms import AddContactForm


@login_required(login_url='/user/login')
def add_contact(request):

    """ Provides a form to add contact and saves it in the database """
    if request.method == 'POST':
        form = AddContactForm(request.POST)
        if form.is_valid():
            contact = form.save()
            return HttpResponseRedirect(reverse("list_contacts"))
    else:
        form = AddContactForm()
    context = RequestContext(request)
    
    return render_to_response("add_contact.html", {'form': form}, context_instance=context)

@login_required(login_url='/user/login/')
def list_contacts(request):
    """ List the contacts added to the database """
    contacts = AddressBook.objects
    return render_to_response("list_contacts.html",
                             {'contacts': contacts},
                             context_instance=RequestContext(request)
                             )


@login_required(login_url='/user/login/')
def delete_contact(request):
    """ Delete the contact with the given id """
    id = eval("request." + request.method + "['id']")
    contact = AddressBook.objects(id=id)[0]
    contact.delete()
    return HttpResponseRedirect(reverse("list_contacts"))

@login_required(login_url='/user/login/')
def update_contact(request):
    """ Provide a form to edit the contact with the given id and save it. """
    id = eval("request." + request.method + "['id']")
    contact = AddressBook.objects(id=id)[0]
    if request.method == 'POST':
        contact.first_name = request.POST['first name']
        contact.last_name = request.POST['last name']
        contact.email = request.POST['email']
        contact.mobile_number = request.POST['mobile number']
        contact.save()
        return HttpResponseRedirect(reverse("list_contacts"))
    else:
        form = AddContactForm(contact)
        return render_to_response("edit_contact.html", {'contact': contact},
                context_instance=RequestContext(request))



