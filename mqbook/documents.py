from mongoengine import *
from datetime import datetime

connect("mqbook")


class AddressBook(Document):
    first_name = StringField(max_length=30, required=True)
    last_name = StringField(max_length=30)
    mobile_number = IntField()
    email = EmailField(max_length=75)
    timestamp = DateTimeField(default=datetime.now)
